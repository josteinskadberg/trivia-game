import {TRIVIA_API} from "./index"

/**
     * Fetches all categories available in the triviaAPI
     * Returns an array containing an error and an array with all categories
     * If error is null, the fetch was successfull
     *  @returns {Array<error,Array>} 
    */
async function apiGetCategories(){
    try{
    const response  = await fetch(`${TRIVIA_API}api_category.php`);
    const categories = await response.json();
    return[null, categories]
}
    catch(error){
        return [error.message, null]
    }
    
}
/**
     * Fetches a token from the triviaAPI
     * Returns an array containing an error and a token
     * If error is null, the fetch was successfull
     * the token is used to ensure that you do not get duplicate questions on subsequent requests of questions
     *  @returns {Array<error,obj>} 
    */
async function apiGetToken(){
    try{
        const response = await fetch(`${TRIVIA_API}api_token.php?command=request`);
        const token = await response.json();
        return [null,token]
    }
    catch(error){
        return [error.message, null]
    }
}
/**
     * Fetches questions based on a params argument
     * Returns an array with error and a object response containing questions and their information
     * If error is null, the fetch was successfull
     *  @returns {Array<error,obj>} 
    */
async function apiGetQuestions(params){
    try{
        const response = await fetch(`${TRIVIA_API}api.php?${params}`); 
        const data  = await response.json();
        return [null, data]
    }
    catch(error){
        return[error.message, null]
    }
}



export {apiGetCategories,apiGetQuestions,apiGetToken}