import { USER_API,USER_API_KEY } from ".";


/**
     * Fetches a user from the userAPI
     * Returns an array with error and userobject
     * If error is null, the fetch was successfull
     *  @returns {Array<error,obj>} 
    */
const getUser = async (username) => {
    
    const link = `${USER_API}?username=${username}`

    try { const response = await fetch(link);
        let data = await response.json();
        if (data.length == 0) {
            return ['User not found', null];
        }
        let user = data[0]

        // some users have score instead of highscore, so have to check and correct the user object for this typo
        if (!('highScore' in user)) {
            if ('score' in user) {
                user.highScore = user.score;
                delete user.score;
            } else { // if no score, default highScore to 0
                user.highScore = 0;F
            }
        } 
        return  [null, user];
    } catch (error) {
        console.error(error);
        return [error, null]
    }
}

/**
     * Registers a user to the userAPI
     * Returns an array with error and userobject
     * If error is null, the registration was successfull
     * The returned userobject contains the id for the new user
     *  @returns {Array<error,obj>} 
    */
const registerUser = async (username) => {
    // if no username provided, do not register user
    if (!username) {
        console.error('No username provided');
        return ['No username provided', null]
    }
    //check if user already exists with same name, if they do, don't register them
    const check_user = await getUser(username);
    if (check_user[0] == null ) {
        console.error('User already exists');
        return ['User already exists', check_user[1]];
    }  

    const API_OPTIONS = {
        method: 'POST',
        headers: {
            'X-API-Key': USER_API_KEY,
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ 
            username: username, 
            highScore: 0 
        })
    };

    try {
    const response = await fetch(USER_API, API_OPTIONS);
    if (!response.ok) {
        console.error('Could not create new user');
        return ['Could not create new user', null];
    }
    const user = await response.json();
    return [null, user];
    } catch(error) {
        console.error(error);
        return [error, null];
    }
}
/**
     * Updates the highscore of a user in the userAPI
     * Returns an array with error and userobject
     * If error is null, the update was successfull
     *  @returns {Array<error,obj>} 
    */
const updateHighScore = async (userId, newHighScore) => {

    const API_OPTIONS = {
        method: 'PATCH', 
        headers: {
            'X-API-Key': USER_API_KEY,
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            highScore: newHighScore  
            //if user originally had score this param will be used from now on
        })
    }
    const link = `${USER_API}/${userId}`

    try {
        const response = await fetch(link, API_OPTIONS);
        if (!response.ok) {
            console.error('Could not update High Score');
            return ['Could not update High Score', null];
        }
        const data = await response.json();
        return [null, data];
    } catch (error) {
        console.error(error);
        return [error, null]
    }

}

export {getUser,registerUser, updateHighScore};