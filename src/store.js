import { createStore } from "vuex";
import {
  apiGetCategories,
  apiGetQuestions,
  apiGetToken,
} from "./api/triviaAPI";
import { updateHighScore } from "./api/userApi";
import he from 'he'


export default createStore({
  state: {
    user: null,
    questions: null,
    token: null,
    categories: null,
    quizParams: null,
    questionIndex: 0,
    submittedAnswers: [],
    shuffledAlternatives: [],
  },
  actions: {

    //Request all availeble qategories from TriviaAPI and saves them to state.
    async fetchCategories({ commit }) {
      const [errormessage, categories] = await apiGetCategories();
      if (errormessage === null) {
        commit("setCategories", categories.trivia_categories);
      } else {
        return null;
      }
    },

    /**Format url params for triviaAPI question request. 
     * Then handle and decode response.
      Saves respones to state and sets correct state for trivia to start */
    async fetchQuestions({ state, commit, dispatch }) {
      console.log("fetch questions");
      if (state.token) {
        const params = new URLSearchParams(state.quizParams).toString();
        const [error, questions] = await apiGetQuestions(params);
        if (error) {
          console.log(error);
          return;
        }
        if (questions.response_code == 0) {
          const result = []
          //decode html encoding
          questions.results.forEach(question => {
            question.question = he.decode(question.question)
            question.correct_answer = he.decode(question.correct_answer)
            question.incorrect_answers = question.incorrect_answers.map(x => he.decode(x))
            result.push(question)
          })
          commit("setQuestions", result);
          commit("setQuestionIndex", 0);
          commit("setSubmittedAnswers", []);
          dispatch("shuffleAlternatives");
        } else {
          console.log(questions.response_code, questions.response_message);
        }
      }
    },

    //get session token for TriviaAPI
    async fetchToken({ commit }) {
      const [error, token] = await apiGetToken();
      if (error) {
        return error;
      }
      if (token.response_code == 0) {
        commit("setToken", token.token);
      }
      return null;
    },

    //iterate the index for current displayed question
    iterateQuestionIndex({ state, commit }) {
      commit("setQuestionIndex", state.questionIndex + 1);
    },

    //replace highscore registered for current user in the userAPI 
    async pushNewHighScore({ state }) {
      let message = await updateHighScore(state.user.id, state.user.highScore);
      if (message[0]) {
        console.error(message[0]);
      }
    },

    //shuffle the order of the alternatives for each question 
    shuffleAlternatives({ state, commit }) {
      let arr = state.questions.map((x) => {
        let answerGroup = [...x.incorrect_answers];
        answerGroup.push(x.correct_answer);
        answerGroup.sort(() => Math.random() - 0.5);
        return answerGroup;
      });
      commit("setShuffledAlternatives", arr);
    }

  },

  mutations: {
    setCategories: (state, payload) => {
      state.categories = payload;
    },
    setQuestions: (state, payload) => {
      state.questions = payload;
    },
    setToken: (state, payload) => {
      state.token = payload;
    },

    setUser: (state, payload) => {
      state.user = payload;
    },
    setQuizParams: (state, payload) => {
      payload.token = state.token;
      state.quizParams = payload;
    },
    setQuestionIndex: (state, payload) => {
      state.questionIndex = payload;
    },
    pushSubmittedAnswer: (state, payload) => {
      state.submittedAnswers.push(payload);
    },
    setHighScore: (state, payload) => {
      state.user.highScore = payload;
    },
    setSubmittedAnswers: (state, payload) => {
      state.submittedAnswers = payload;
    },
    setShuffledAlternatives: (state, payload) => {
      state.shuffledAlternatives = payload;
    }
  },

  getters: {

    /**
     * Get question text for each question
     *  @returns {Array<string>} 
    */
    questions: (state) => {
      return state.questions.map((x) => x.question);
    },

    /**
     * Get an array of objects containing necessary information for calculating result
     *  @returns {Array<obj>} 
    */
    questionsAndAnswers: (state) => {
      let arr = [];
      for (let i = 0; i < state.questions.length; i++) {
        let q = state.questions[i];
        const obj = {
          index: i,
          question: q.question,
          correct_answer: q.correct_answer,
          alternatives: state.shuffledAlternatives[i],
          answer: state.submittedAnswers[i],
        };
        arr.push(obj);
      }
      return arr;
    },
  },
});
