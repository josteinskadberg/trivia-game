import { createRouter, createWebHashHistory } from "vue-router"
import StartScreen from "./views/StartScreen.vue"
import Quiz from "./views/Quiz.vue"
import Results from "./views/Results.vue"

const routes = [
    {
        path: "/",
        component : StartScreen
    },
    {
        path: "/quiz",
        component : Quiz
    },
    {
        path: "/results",
        component : Results
    },
] 


export default createRouter({
    history: createWebHashHistory(),
    routes 
})

