module.exports = {
  content: [
    "./indexedDB.html",
    "./src/**/*.{vue,js}"
  ],
  theme: {
    extend: {},
    colors:{
     "purple": "#233582",
      "matt": "#553D67",
      "pink": "#F64C72",
      "light-matt":"#99738E",
      "light-purple": "#2F2FA2",
      "ex-light-purple": "#6360DB",
      "white": "#FFFFFF",
      "green": "#18C91E",
      "red": "#EB094D",
      "yellow": "#EDF511",
      "dark-green": "#16B51E"
    }
  },
  plugins: [],
}