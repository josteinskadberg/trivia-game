# trivia-game
heroku-url: https://our-trivia-game.herokuapp.com/

## Contributors:
Jostein Skadberg - gitlab.com/josteinskadberg 

Jonas Svåsand - gitlab.com/jsva

## Description:
A trivia game that uses the open trivia API (https://opentdb.com/).
You can chose the amount of questions, the category, difficulty and type of question you will get.
Your answers will scored, and your highest score is saved to your username.
In the results screen you  can see what you answered correctly (green) and incorrectly (red), as well as your score.

## Instructions

**You need to have npm installed**

1. clone this repo
2. navigate to this folder and open a command prompt
3. `npm install`
4. `npm run dev`
5. Go to the the adress, provided in the command prompt, in your browser.
6. You can now use the app.
7. Enter a user (can not be empty). Usernames are case sensitive, and unique 
8. Choose the parameters you want for the quiz and click start quiz
9. After finishing a quiz you can go again with the same parameters by using the "Play again" button or choose new by using "Home" (requires you to login again)

Alternatively you can play by using the website url provided at the top.

If you want to run your own instance of the userAPI, replace USER_API and USER_API_KEY in index.js in the api folder.
API-keys are currently not gitignored for grading convinience. 

